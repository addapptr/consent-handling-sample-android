# Consent handling samples for Addapptr

Apps on this repository demonstrate ways of handling GDPR consent using AATKit.
See also the [wiki](https://aatkit.gitbook.io/android-integration/start/consent) for more instructions.

---
## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@bitbucket.org:addapptr/consent-handling-sample-android.git
```
---
## Maintainers
Current maintainers:

* [Damian Supera](https://bitbucket.org/damian_s/)
* [Michał Kapuściński](https://bitbucket.org/m_kapuscinski/)
